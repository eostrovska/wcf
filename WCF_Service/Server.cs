﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using WCF_Service;

namespace WCF_Service
{
    class Server
    {
        static void Main(string[] args)
        {
            Console.Title = "SERVER";

            ServiceHost serviceHost = new ServiceHost(typeof(Service), new Uri("http://localhost:5000/ICalculator"));

            serviceHost.AddServiceEndpoint(typeof(ICalculator), new BasicHttpBinding(), "");
            serviceHost.Open();

            Console.WriteLine("The server was successfully started.");
            Console.ReadKey();

            serviceHost.Close();
        }
    }
}
