﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCF_Service
{
    class Service : ICalculator
    {
        public int Multiplication(int firstNum, int secondNum)
        {
            Console.WriteLine("Multiplication was done.");
            return firstNum * secondNum;
        }
      
        public int Addition(int firstNum, int secondNum)
        {
            Console.WriteLine("Addition was done.");
            return firstNum + secondNum; 
        }

        public float Division(int firstNum, int secondNum)
        {
            if(secondNum == 0)
            {
                Console.WriteLine("Division by zero is impossible!");
            }

            Console.WriteLine("Division was done.");
            return (float)firstNum / secondNum;
        }

        public int Subtraction(int firstNum, int secondNum)
        {
            Console.WriteLine("Subtraction was done.");
            return firstNum - secondNum;
        }
    }
}
