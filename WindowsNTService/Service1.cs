﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration.Install;

namespace WindowsNTService
{
    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;

            service = new ServiceInstaller();
            service.ServiceName = "----- NTService_Calculator -----";
            service.Description = "Calculator";
            service.StartType = ServiceStartMode.Automatic;

            Installers.Add(process);
            Installers.Add(service);
        }
    }

    public partial class Service1 : ServiceBase
    {
        ServiceHost service = null;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (service == null)
            {
                service = new ServiceHost(typeof(Service));
                service.Open();
            }
        }

        protected override void OnStop()
        {
            if (service != null)
            {
                service.Close();
            }
        }
    }

    class Service : ICalculator
    {
        public int Multiplication(int firstNum, int secondNum)
        {
            Console.WriteLine("Multiplication was done.");
            return firstNum * secondNum;
        }

        public int Addition(int firstNum, int secondNum)
        {
            Console.WriteLine("Addition was done.");
            return firstNum + secondNum;
        }

        public float Division(int firstNum, int secondNum)
        {
            if (secondNum == 0)
            {
                Console.WriteLine("Division by zero is impossible!");
            }

            Console.WriteLine("Division was done.");
            return (float)firstNum / secondNum;
        }

        public int Subtraction(int firstNum, int secondNum)
        {
            Console.WriteLine("Subtraction was done.");
            return firstNum - secondNum;
        }
    }
}

