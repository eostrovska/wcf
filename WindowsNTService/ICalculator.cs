﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WindowsNTService
{
    [ServiceContract]
    interface ICalculator
    {
        [OperationContract]
        int Multiplication(int firstNum, int secondNum);

        [OperationContract]
        int Addition(int firstNum, int secondNum);

        [OperationContract]
        float Division(int firstNum, int secondNum);

        [OperationContract]
        int Subtraction(int firstNum, int secondNum);
    }
}
