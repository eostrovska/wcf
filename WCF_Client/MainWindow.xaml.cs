﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WCF_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Uri address = new Uri("http://localhost:5000/ICalculator");
        BasicHttpBinding binding = new BasicHttpBinding();
        ChannelFactory<ICalculator> factory;
        ICalculator channel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void additionButton_Click(object sender, RoutedEventArgs e)
        {
            int firstNum = Int32.Parse(firstNumTextBox.Text);
            int secondNum = Int32.Parse(secondNumTextBox.Text);

            try
            {
                if (factory == null)
                {
                    factory = new ChannelFactory<ICalculator>(binding, new EndpointAddress(address));
                    channel = factory.CreateChannel();
                }

                if (factory != null && channel != null)
                {
                    int responceAddition = channel.Addition(firstNum, secondNum);
                    AdditionResultTextBox.Text = responceAddition.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void multiplicationButton_Click(object sender, RoutedEventArgs e)
        {
            int firstNum = Int32.Parse(firstNumTextBox.Text);
            int secondNum = Int32.Parse(secondNumTextBox.Text);

            try
            {
                if (factory == null)
                {
                    factory = new ChannelFactory<ICalculator>(binding, new EndpointAddress(address));
                    channel = factory.CreateChannel();
                }

                if (factory != null && channel != null)
                {
                    int responceMultiplication = channel.Multiplication(firstNum, secondNum);
                    MultiplicationResultTextBox.Text = responceMultiplication.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void divisionButton_Click(object sender, RoutedEventArgs e)
        {
            int firstNum = Int32.Parse(firstNumTextBox.Text);
            int secondNum = Int32.Parse(secondNumTextBox.Text);

            try
            {
                if (factory == null)
                {
                    factory = new ChannelFactory<ICalculator>(binding, new EndpointAddress(address));
                    channel = factory.CreateChannel();
                }

                if (factory != null && channel != null)
                {
                    if(secondNum != 0)
                    {
                        float responceDivision = channel.Division(firstNum, secondNum);
                        DivisionResultTextBox.Text = responceDivision.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Division by zero is imposible!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void subtractionButton_Click(object sender, RoutedEventArgs e)
        {
            int firstNum = Int32.Parse(firstNumTextBox.Text);
            int secondNum = Int32.Parse(secondNumTextBox.Text);

            try
            {
                if (factory == null)
                {
                    factory = new ChannelFactory<ICalculator>(binding, new EndpointAddress(address));
                    channel = factory.CreateChannel();
                }

                if (factory != null && channel != null)
                {
                    int responceSubtraction = channel.Subtraction(firstNum, secondNum);
                    SubtractionResultTextBox.Text = responceSubtraction.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
