How to start App

 Open the project and build on the release

1. Open a command prompt as an administrator
2. Type the following at the command prompt: cd C:\Windows\Microsoft.NET\Framework\v4.0.30319 and press Enter
3. Enter InstallUtil.exe, the spacing, the address for storing the .exe file of the ServiceNT and press Enter
4. Open the Windows Services and start the new ServiceNT by clicking on it
5. Run the .exe file of the Client (in the project folder) as the administrator
6. Use the application
7. If the ServiceNT is not needed - remove it with the help of the command line: enter the InstallUtil.exe/u and the address of the ServiceNT